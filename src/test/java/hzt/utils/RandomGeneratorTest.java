package hzt.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RandomGeneratorTest {

    @ParameterizedTest
    @ValueSource(doubles = {-45.6, 3423, 563, 1214, -23423.3})
    void testRandomDoubleGreaterThanOrEqualToLowerBound(double lowerBound) {
        double result = RandomGenerator.randomDouble(lowerBound, Double.MAX_VALUE);
        assertTrue(result >= lowerBound);
    }

    @ParameterizedTest
    @ValueSource(doubles = {-45.6, 3423, 563, 1214, -23423.3})
    void testRandomDoubleSmallerThanOrEqualToUpperBound(double upperBound) {
        double result = RandomGenerator.randomDouble(-Double.MAX_VALUE, upperBound);
        assertTrue(result <= upperBound);
    }

    @Test
    void testThrowsIllegalStateExceptionWhenLowerBoundGreaterThanUpperBound() {
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class,
                () -> RandomGenerator.randomDouble(3, 2));
        assertEquals("lowerBound greater than upper bound", illegalStateException.getMessage());
    }

}
