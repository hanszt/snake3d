package hzt.model.fxcustoms.inputtarget;

import hzt.model.fxcustoms.MyScene;
import hzt.model.fxcustoms.MyVBox;
import javafx.scene.Group;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InteractableTest {

    @Test
    @Disabled("Needs javafx test framework")
    void testInteractableCanBeUsedForBothNodeAndScene() {
        MyScene scene = new MyScene(new Group());
        MyVBox vBox = new MyVBox();
        setup(scene, vBox);

        assertTrue(scene.onMouseClickedProperty().isBound());
        assertFalse(vBox.onMouseReleasedProperty().isBound());
    }

    public void setup(Interactable interactable, Interactable reference) {
        interactable.onMouseClickedProperty().bind(reference.onMouseReleasedProperty());
    }
}
