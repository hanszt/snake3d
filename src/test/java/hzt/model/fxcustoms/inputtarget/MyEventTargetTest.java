package hzt.model.fxcustoms.inputtarget;

import hzt.model.fxcustoms.MyScene;
import hzt.model.fxcustoms.MyStage;
import hzt.model.fxcustoms.MyVBox;
import javafx.scene.Group;
import javafx.scene.input.KeyEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class MyEventTargetTest {

    @Test
    @Disabled("Needs javafx test framework")
    void testEventTarget() {
        Assertions.assertDoesNotThrow(() -> printOnKeyPressed(new MyStage()));
        Assertions.assertDoesNotThrow(() -> printOnKeyPressed(new MyScene(new Group())));
        Assertions.assertDoesNotThrow(() -> printOnKeyPressed(new MyVBox()));
    }

    public void printOnKeyPressed(MyEventTarget eventTarget) {
        eventTarget.addEventHandler(KeyEvent.KEY_PRESSED, (e) -> System.out.println("hallo"));
    }

}
