package hzt.model;

import hzt.model.snake.Snake;
import javafx.geometry.Point3D;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SnakeTest {

    private final Snake snake = new Snake();
    private final Dimension3D halfObjectDimension = new Dimension3D(1, 1, 1);

    private SnakeTest() {
        snake.segmentSizeProperty().set(10);
    }

    @ParameterizedTest
    @ValueSource(doubles = {5, 15, 10, 12, 8})
    void testHeadIntersectsWithObjectXParameterized(double translateX) {
        var head = snake.getHead();
        head.setTranslateX(10);
        var point = new Point3D(translateX, 0, 0);
        assertTrue(snake.objectInSnakeHead(point, halfObjectDimension));
    }

    @ParameterizedTest
    @ValueSource(doubles = {15, 16, 20, 23, 25})
    void testHeadIntersectsWithObjectZParameterized(double translateZ) {
        var head = snake.getHead();
        head.setTranslateZ(20);
        var midPoint = new Point3D(0, 0, translateZ);
        assertTrue(snake.objectInSnakeHead(midPoint, halfObjectDimension));
    }

    @ParameterizedTest
    @ValueSource(doubles = {4, 16, 17, -12, 50})
    void testHeadNotIntersectsWithObjectXParameterized(double translateX) {
        var head = snake.getHead();
        head.setTranslateX(10);
        var point = new Point3D(translateX, 0, 0);
        assertFalse(snake.objectInSnakeHead(point, halfObjectDimension));
    }

    @ParameterizedTest
    @ValueSource(doubles = {4, 14, 26, -12, 50})
    void testHeadNotIntersectsWithObjectZParameterized(double translateZ) {
        var head = snake.getHead();
        head.setTranslateZ(20);
        var point = new Point3D(0, 0, translateZ);
        assertFalse(snake.objectInSnakeHead(point, halfObjectDimension));
    }
}
