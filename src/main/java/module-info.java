open module Main.app {

    requires javafx.controls;
    requires org.jetbrains.annotations;
    requires org.slf4j;
    requires javafx.fxml;
}
