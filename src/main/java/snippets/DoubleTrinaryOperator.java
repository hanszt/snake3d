package snippets;

@FunctionalInterface
public interface DoubleTrinaryOperator {

    double applyAsDouble(double var1, double var2, double var3);

}
