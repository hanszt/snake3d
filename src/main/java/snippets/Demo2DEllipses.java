package snippets;

import hzt.model.SogetiLogo;
import hzt.utils.builders.animation.Transitions;
import hzt.utils.builders.shape.EllipseBuilder;
import javafx.animation.Animation;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static hzt.utils.RandomGenerator.randomColor;
import static hzt.utils.RandomGenerator.randomDouble;

public class Demo2DEllipses extends Application {

    public static final int SCENE_WIDTH = 800;
    public static final int SCENE_HEIGHT = 600;
    public static final int NR_OF_ELLIPSES = 100;

    @Override
    public void start(Stage stage) {
        var root = new Group(buildEllipses());
        SogetiLogo sogetiLogo = new SogetiLogo();
        addTranslateTransition(sogetiLogo);
        root.getChildren().add(sogetiLogo);
        sogetiLogo.getShapes().forEach(Demo2DEllipses::addFadeTransition);
        Scene scene = new Scene(root, SCENE_WIDTH, SCENE_HEIGHT);
        configureStage(stage);
        stage.setScene(scene);
        stage.show();
    }

    private void configureStage(Stage stage) {
        stage.setTitle("Confetti demo");
        final var icons = stage.getIcons();
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(icons::add);
    }

    private static Collection<Node> buildEllipses() {
        return IntStream.range(0, NR_OF_ELLIPSES)
                .mapToObj(Demo2DEllipses::buildEllipse)
                .collect(Collectors.toUnmodifiableSet());
    }

    private static void addFillTransition(Shape shape) {
        Transitions.fillTransition(Duration.seconds(randomDouble(5, 20)))
                .forShape(shape)
                .fromValue(randomColor())
                .toValue(randomColor())
                .withAutoReverse(true)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

    private static void addFadeTransition(Shape shape) {
        Transitions.fadeTransition(Duration.seconds(randomDouble(5, 20)))
                .forNode(shape)
                .fromValue(Math.random())
                .toValue(Math.random())
                .withAutoReverse(true)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

    private static void addTranslateTransition(Node node) {
        Transitions.translateTransition(Duration.seconds(randomDouble(5, 20)), node)
                .fromX(randomDouble(0, SCENE_WIDTH))
                .toX(SCENE_WIDTH / 2.)
                .fromY(randomDouble(0, SCENE_WIDTH))
                .toY(SCENE_HEIGHT / 2.)
                .withAutoReverse(true)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

    private static void addRotateTransition(Node node) {
        Transitions.rotateTransition(Duration.seconds(randomDouble(5, 20)), node)
                .fromAngle(-90)
                .toAngle(90)
                .withAutoReverse(true)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

    @NotNull
    private static Ellipse buildEllipse(int i) {
        final var ellipse = EllipseBuilder.ellipse(
                        randomDouble(1, 10),
                        randomDouble(1, 10))
                .withFill(Color.color(Math.random(), Math.random(), Math.random()))
                .withTranslation(randomDouble(0, SCENE_WIDTH), randomDouble(0, SCENE_HEIGHT))
                .build();

        addRotateTransition(ellipse);
        addTranslateTransition(ellipse);
        addFillTransition(ellipse);
        addFadeTransition(ellipse);
        return ellipse;
    }
}
