package snippets;

import hzt.model.SogetiLogo;
import hzt.model.fxcustoms.MySubScene;
import hzt.model.fxcustoms.MyVBox;
import hzt.service.MouseControlService;
import hzt.service.interfaces.IMouseControlService;
import hzt.utils.FxUtils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.function.DoubleBinaryOperator;

import static hzt.utils.RandomGenerator.randomColor;
import static java.lang.Math.*;

public class Function3DVisualizer extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Function3DVisualizer.class);

    public static final int SCENE3D_HEIGHT = 400;
    public static final int SCENE3D_WIDTH = 600;
    public static final int SIZE = 100;

    private final Group root3D = new Group();
    private final IMouseControlService mouseControlService = new MouseControlService();
    private final Slider pointSizeSlider = new Slider();
    private final Slider velocitySlider = new Slider();
    private final ComboBox<Function3D> functionComboBox = new ComboBox<>();
    private final ColorPicker colorPicker = new ColorPicker();
    private final Shape3D[][] shape3DGrid = new Shape3D[SIZE][SIZE];

    @Override
    public void start(Stage stage) {
        LOGGER.info("{} starting...", getClass().getSimpleName());
        configureControls();
        MySubScene subScene = buildSubScene();
        SogetiLogo sogetiLogo = new SogetiLogo();
        sogetiLogo.setScale(.5);
        Button button = new Button("Exit");
        button.setOnAction(e -> stage.close());
        MyVBox vBox = new MyVBox(subScene, pointSizeSlider, velocitySlider,
                new HBox(button, colorPicker, functionComboBox, sogetiLogo));
        vBox.setSpacing(5);
        Scene scene = new Scene(vBox, 600, 600);
        scene.setOnKeyTyped(this::keyTypedAction);

        mouseControlService.initMouseControl(root3D, vBox);

        stage.setScene(scene);
        configureStage(stage);
        stage.show();
        LOGGER.info("{} started", getClass().getSimpleName());
    }
    private void configureControls() {
        pointSizeSlider.setValue(.5);
        pointSizeSlider.setMax(2);
        velocitySlider.setValue(1);
        Function3D sineWave = new Function3D("sin(t + x) + cos(t + y)", (x, y, t) -> sin(t + x) + cos(t + y));
        Function3D strange = new Function3D("sin(t + x) * sqrt(y)", (x, y, t) ->  sin(t + x) * sqrt(y));
        Function3D strange2 = new Function3D("sin(t + x) * sqrt(y)", (x, y, t) ->  sin(t + x) * tan(y));
        functionComboBox.setValue(sineWave);
        functionComboBox.getItems().addAll(sineWave, strange, strange2);
        colorPicker.setValue(Color.RED);
    }
    private record Function3D(String name, DoubleTrinaryOperator function) {

        @Override
        public String toString() {
            return name;
        }

    }

    private void configureStage(Stage stage) {
        stage.setTitle(getClass().getSimpleName());
        final var icons = stage.getIcons();
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(icons::add);
    }

    private void keyTypedAction(KeyEvent e) {
        if ("w".equals(e.getCharacter())) {
            FxUtils.allDescendents(root3D).stream()
                    .filter(Shape3D.class::isInstance)
                    .map(Shape3D.class::cast)
                    .forEach(FxUtils::setDrawMode);
        }
    }

    @NotNull
    private MySubScene buildSubScene() {
        Group mathFunction = build3DMathFunction((x, y) -> sin(x) + cos(y));
        var timeline = new Timeline(new KeyFrame(Duration.millis(20), e -> run()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        mathFunction.setRotationAxis(Rotate.X_AXIS);
        mathFunction.setRotate(90);
        root3D.getChildren().add(mathFunction);
        PerspectiveCamera camera = buildCamera();
        var subScene = new MySubScene(root3D, SCENE3D_WIDTH, SCENE3D_HEIGHT, true, SceneAntialiasing.BALANCED);
        subScene.setCamera(camera);
        return subScene;
    }

    private void run() {
        updateDynamic3DMathFunction(functionComboBox.getValue().function);
    }

    @NotNull
    private static PerspectiveCamera buildCamera() {
        PerspectiveCamera camera = new PerspectiveCamera(true);
        camera.setFarClip(10_000);
        camera.setTranslateZ(-200);
        return camera;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private Group build3DMathFunction(DoubleBinaryOperator biFunction) {
        Group group = new Group();
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                int xPos = x - SIZE / 2;
                int yPos = y - SIZE / 2;
                double zPos = biFunction.applyAsDouble(x, y);
                var point = buildSphere();
                point.setTranslateX(xPos);
                point.setTranslateY(yPos);
                point.setTranslateZ(zPos);
                shape3DGrid[x][y] = point;
                group.getChildren().add(point);
            }
        }
        return group;
    }

    private void updateDynamic3DMathFunction(DoubleTrinaryOperator triFunction) {
        double speed = velocitySlider.getValue();
        double timeInSeconds = speed * (System.currentTimeMillis() / 1e3);
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                double z = triFunction.applyAsDouble(x, y, timeInSeconds);
                var point = shape3DGrid[x][y];
                point.setTranslateZ(z);
            }
        }
    }

    @NotNull
    private Sphere buildSphere() {
        var shape3D = new Sphere();
        shape3D.radiusProperty().bindBidirectional(pointSizeSlider.valueProperty());
        PhongMaterial material = new PhongMaterial(randomColor());
        material.setSpecularColor(Color.WHITE);
        material.diffuseColorProperty().bindBidirectional(colorPicker.valueProperty());
        shape3D.setMaterial(material);
        return shape3D;
    }

}
