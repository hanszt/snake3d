package snippets;

import hzt.model.SogetiLogo;
import hzt.model.fxcustoms.MySubScene;
import hzt.model.fxcustoms.MyVBox;
import hzt.service.MouseControlService;
import hzt.service.interfaces.IMouseControlService;
import hzt.utils.FxUtils;
import hzt.utils.RandomGenerator;
import hzt.utils.builders.animation.Transitions;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static hzt.utils.RandomGenerator.randomColor;
import static hzt.utils.RandomGenerator.randomDouble;

public class Object3DBoundToSlider extends Application {

    private static final Random RANDOM = new Random();
    public static final int SCENE3D_HEIGHT = 400;
    public static final int SCENE3D_WIDTH = 600;
    public static final int BOUND = 200;
    public static final int NR_OF_SHAPES3D = 100;

    private final IMouseControlService mouseControlService = new MouseControlService();

    private final Group root3D = new Group();
    private final Slider slider = new Slider();
    private final ColorPicker colorPicker = new ColorPicker();

    @Override
    public void start(Stage stage) {
        configureControls();
        var subScene = buildSubScene();
        var sogetiLogo = new SogetiLogo();
        sogetiLogo.setScale(.5);
        var button = new Button("Exit");
        button.setOnAction(e -> stage.close());
        button.setMinWidth(100);

        var controlsPane = new HBox(button, colorPicker, sogetiLogo);
        controlsPane.setSpacing(10);

        var stackPane = new StackPane(subScene);
        var root = new MyVBox(stackPane, slider, controlsPane);
        root.setSpacing(5);
        root.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
        root.setStyle("-fx-base: rgb(40, 40, 40);-fx-background: rgb(60, 60, 60);");

        var scene = new Scene(root, 600, 600);
        scene.setOnKeyTyped(this::keyTypedAction);
        subScene.widthProperty().bind(scene.widthProperty());
        subScene.heightProperty().bind(scene.heightProperty().subtract(200));
        mouseControlService.initMouseControl(root3D, root);

        stage.setScene(scene);
        configureStage(stage);
        FxUtils.addIconToStage(stage);
        stage.show();
    }

    private void configureControls() {
        slider.setValue(10);
        colorPicker.setValue(Color.RED);
        colorPicker.setMinWidth(100);
    }

    private void configureStage(Stage stage) {
        stage.setTitle(getClass().getSimpleName());
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(stage.getIcons()::add);
    }

    private void keyTypedAction(KeyEvent e) {
        if ("w".equals(e.getCharacter())) {
            root3D.getChildren().stream()
                    .filter(Shape3D.class::isInstance)
                    .map(Shape3D.class::cast)
                    .forEach(FxUtils::setDrawMode);
        }
    }

    @NotNull
    private MySubScene buildSubScene() {
        root3D.getChildren().addAll(buildRandomShapes());
        var subScene = new MySubScene(root3D, SCENE3D_WIDTH, SCENE3D_HEIGHT, true, SceneAntialiasing.BALANCED);
        subScene.setCamera(buildCamera());
        return subScene;
    }

    private Collection<Node> buildRandomShapes() {
        List<Supplier<Shape3D>> shape3DSupplierList = List.of(this::buildBox, this::buildCylinder, this::buildSphere);
        return IntStream.range(0, NR_OF_SHAPES3D)
                .mapToObj(i -> buildRandomShape3D(shape3DSupplierList))
                .collect(Collectors.toUnmodifiableSet());
    }

    private static Shape3D buildRandomShape3D(List<Supplier<Shape3D>> shape3DSupplierList) {
        return shape3DSupplierList.get(RANDOM.nextInt(shape3DSupplierList.size())).get();
    }

    @NotNull
    private Sphere buildSphere() {
        var shape3D = new Sphere();
        shape3D.radiusProperty().bindBidirectional(slider.valueProperty());
        addMaterialAndTransitions(shape3D);
        return shape3D;
    }

    @NotNull
    private Cylinder buildCylinder() {
        var shape3D = new Cylinder();
        shape3D.heightProperty().bind(slider.valueProperty().divide(2));
        shape3D.radiusProperty().bindBidirectional(slider.valueProperty());
        addMaterialAndTransitions(shape3D);
        return shape3D;
    }

    @NotNull
    private Box buildBox() {
        var box = new Box();
        box.widthProperty().bindBidirectional(slider.valueProperty());
        box.heightProperty().bind(slider.valueProperty().divide(2));
        box.depthProperty().bindBidirectional(slider.valueProperty());
        var material = addMaterialAndTransitions(box);
        material.diffuseColorProperty().bindBidirectional(colorPicker.valueProperty());
        return box;
    }

    private static PhongMaterial addMaterialAndTransitions(Shape3D shape3D) {
        var material = new PhongMaterial(randomColor());
        material.setSpecularColor(Color.WHITE);
        shape3D.setMaterial(material);
        addTranslateTransition(shape3D);
        addRotateTransition(shape3D);
        return material;
    }

    @NotNull
    private static PerspectiveCamera buildCamera() {
        var camera = new PerspectiveCamera(true);
        camera.setFarClip(10_000);
        camera.setTranslateZ(-400);
        return camera;
    }

    private static void addRotateTransition(Node node) {
        Transitions.rotateTransition(Duration.seconds(randomDouble(5, 20)), node)
                .fromAngle(0)
                .toAngle(360)
                .withAxis(RandomGenerator.randomAngle())
                .withInterpolator(Interpolator.LINEAR)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

    private static void addTranslateTransition(Node node) {
        Transitions.translateTransition(Duration.seconds(randomDouble(5, 20)), node)
                .fromX(randomDouble(-BOUND, BOUND))
                .fromY(randomDouble(-BOUND, BOUND))
                .fromZ(randomDouble(-BOUND, BOUND))
                .toX(0)
                .toY(0)
                .toZ(0)
                .withAutoReverse(true)
                .withCycleCount(Animation.INDEFINITE)
                .play();
    }

}
