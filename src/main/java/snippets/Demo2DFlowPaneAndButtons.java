package snippets;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.Optional;
import java.util.stream.IntStream;

import static hzt.utils.FxUtils.multiple;

public class Demo2DFlowPaneAndButtons extends Application {

    public static final int NR_OF_BUTTONS = 100;

    @Override
    public void start(Stage stage) {
        var root = new FlowPane();

        root.setStyle("-fx-base: rgb(40, 40, 40);\n" +
                "    -fx-background: rgb(60, 60, 60);");

        root.getChildren().addAll(IntStream.range(0, NR_OF_BUTTONS)
                .mapToObj(Demo2DFlowPaneAndButtons::toButtonVBox)
                .toList());

        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        configureStage(stage);
        stage.show();
    }

    private void configureStage(Stage stage) {
        stage.setTitle(getClass().getSimpleName());
        final var icons = stage.getIcons();
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(icons::add);
    }

    private static HBox toButtonVBox(int index) {
        var button = new Button("Print Hello " + index);
        var label = new Label();
        button.setOnAction(multiple(
                () -> label.setText("Hello FX! " + index),
                Demo2DFlowPaneAndButtons::printHello));
        return new HBox(button, label);
    }

    private static void printHello() {
        System.out.println("Hello");
    }

}
