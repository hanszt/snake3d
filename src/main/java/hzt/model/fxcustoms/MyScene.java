package hzt.model.fxcustoms;

import hzt.model.fxcustoms.inputtarget.Interactable;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class MyScene extends Scene implements Interactable {

    public MyScene(Parent root, double width, double height) {
        super(root, width, height);
    }

    public MyScene(Parent parent) {
        super(parent);
    }
}
