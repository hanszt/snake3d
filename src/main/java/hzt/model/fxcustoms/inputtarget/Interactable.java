package hzt.model.fxcustoms.inputtarget;

public interface Interactable extends MyEventTarget, MouseTarget, KeyEventTarget,
        InputMethodEventTarget, RotateEventTarget, ZoomEventTarget, SwipeEventTarget, TouchEventTarget,
        DragEventTarget, ContextMenuEventTarget {

}
