package hzt.model.fxcustoms.inputtarget;

import javafx.event.EventTarget;

public interface MyEventTarget extends EventTarget, EventFilterTarget, EventHandlerTarget, EventDispatcherTarget {
}
