package hzt.model.fxcustoms.inputtarget;

public interface MouseTarget extends MouseEventTarget, MouseDragEventTarget, ScrollEventTarget {

}
