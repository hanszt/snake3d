package hzt.model.snake;

import hzt.model.Dimension3D;
import hzt.model.fxcustoms.inputtarget.Interactable;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Group;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;

public abstract class BodySegment extends Group implements Interactable {

    private final Shape3D shape3D;
    private final PhongMaterial material;

    protected BodySegment(Shape3D shape3D) {
        this.shape3D = shape3D;
        this.material = new PhongMaterial();
        shape3D.setMaterial(material);
        super.getChildren().add(shape3D);
    }

    public static BodySegment buildSphereSegment() {
        return new SphereBodySegment();
    }

    public static BodySegment buildBoxSegment() {
        return new BoxBodySegment();
    }

    public Shape3D getShape3D() {
        return shape3D;
    }

    public PhongMaterial getMaterial() {
        return material;
    }

    abstract void bindToSegmentSize(DoubleProperty segmentSize);

    abstract Dimension3D halfDimension();

    @Override
    public abstract String toString();

}
