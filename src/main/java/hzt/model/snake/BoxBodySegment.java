package hzt.model.snake;

import hzt.model.Dimension3D;
import javafx.beans.property.DoubleProperty;
import javafx.scene.shape.Box;

public class BoxBodySegment extends BodySegment {


    public BoxBodySegment() {
        super(new Box());
    }

    public void bindToSegmentSize(DoubleProperty segmentSize) {
        var box = (Box) getShape3D();
        box.widthProperty().bind(segmentSize);
        box.heightProperty().bind(segmentSize);
        box.depthProperty().bind(segmentSize);
    }

    @Override
    Dimension3D halfDimension() {
        var box = (Box) getShape3D();
        return new Dimension3D(box.getWidth() / 2, box.getHeight() / 2, box.getDepth() / 2);
    }

    @Override
    public String toString() {
        return "Box body";
    }
}
