package hzt.model.snake;

import hzt.model.Dimension3D;
import javafx.beans.property.DoubleProperty;
import javafx.scene.shape.Sphere;

public class SphereBodySegment extends BodySegment {

    public SphereBodySegment() {
        super(new Sphere());
    }

    @Override
    void bindToSegmentSize(DoubleProperty segmentSize) {
        ((Sphere) getShape3D()).radiusProperty().bind(segmentSize.divide(2));
    }

    @Override
    Dimension3D halfDimension() {
        var sphere = (Sphere) getShape3D();
        var radius = sphere.getRadius();
        return new Dimension3D(radius, radius, radius);
    }

    @Override
    public String toString() {
        return "Sphere snake";
    }
}
