package hzt.model.snake;

import hzt.model.Dimension3D;
import hzt.model.fxcustoms.inputtarget.Interactable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import java.util.stream.IntStream;

import static java.util.function.Predicate.*;

public class Snake extends Group implements Interactable {

    public static final int INIT_SNAKE_SEGMENT_SIZE = 40;
    private static final int DEFAULT_NR_BODY_SEGMENTS = 8;
    private static final Point2D INIT_DIRECTION = new Point2D(-1, 0);

    private final ObjectProperty<Color> color = new SimpleObjectProperty<>();
    private final DoubleProperty segmentSize = new SimpleDoubleProperty(INIT_SNAKE_SEGMENT_SIZE);
    private final ObjectProperty<Point2D> direction = new SimpleObjectProperty<>(INIT_DIRECTION);
    private final BooleanProperty alive = new SimpleBooleanProperty(true);
    private final BodySegment head;

    private Point2D prevDirection = direction.get();

    public Snake() {
        this(DEFAULT_NR_BODY_SEGMENTS);
    }

    public Snake(int initNrOfSegments) {
        this.color.set(Color.RED);
        this.head = buildAndAddColor(Point2D.ZERO);
        super.getChildren().add(head);
        buildBody(initNrOfSegments);
    }

    private void buildBody(int initNrOfSegments) {
        IntStream.range(1, initNrOfSegments)
                .mapToObj(i -> buildAndAddColor(new Point2D(i * this.segmentSize.get(), 0)))
                .forEach(body -> getChildren().add(body));
    }

    private BodySegment buildAndAddColor(Point2D position) {
        var segment = BodySegment.buildSphereSegment();
        segment.bindToSegmentSize(segmentSize);
        segment.setTranslateX(position.getX());
        segment.setTranslateZ(position.getY());
        segment.translateYProperty().bind(segmentSize.divide(-2));
        segment.getMaterial().diffuseColorProperty().bind(color);
        return segment;
    }

    public void grow() {
        var size = getChildren().size();
        var last = getChildren().get(size - 1);
        var secondLast = getChildren().get(size - 2);
        var positionSecondLast = new Point2D(secondLast.getTranslateX(), secondLast.getTranslateZ());
        var positionLast = new Point2D(last.getTranslateX(), last.getTranslateZ());
        var diff = positionSecondLast.subtract(positionLast);
        var newPosition = positionLast.add(diff);
        getChildren().add(buildAndAddColor(newPosition));
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public DoubleProperty segmentSizeProperty() {
        return segmentSize;
    }

    public void update() {
        updateXZPositionOtherBodySegments();
        updateXYPositionHead();
        if (intersectsSelf()) {
            alive.set(false);
        }
    }

    private void updateXYPositionHead() {
        Point2D newDir = direction.get();
        boolean oppositeDir = newDir.multiply(-1).equals(prevDirection);
        if (oppositeDir) {
            newDir = prevDirection;
        }
        head.setTranslateX(head.getTranslateX() + (segmentSize.get() * newDir.getX()));
        head.setTranslateZ(head.getTranslateZ() + (segmentSize.get() * newDir.getY()));
        prevDirection = newDir;
    }

    private void updateXZPositionOtherBodySegments() {
        //from back to front
        for (int i = getChildren().size() - 1; i >= 1; i--) {
            var current = getChildren().get(i);
            var inFront = getChildren().get(i - 1);
            current.setTranslateX(inFront.getTranslateX());
            current.setTranslateZ(inFront.getTranslateZ());
        }
    }

    public void updateDir(KeyEvent e) {
        switch (e.getCharacter()) {
            case "w" -> direction.set(new Point2D(0, 1));
            case "s" -> direction.set(new Point2D(0, -1));
            case "a" -> direction.set(new Point2D(-1, 0));
            case "d" -> direction.set(new Point2D(1, 0));
            default -> {
            }
        }
    }

    public int size() {
        return getChildren().size();
    }

    public boolean objectInSnakeHead(Point3D centerPoint3D, Dimension3D objectDimension) {
        var objectHalfDim = objectDimension.multiply(.5);
        var positionHead = new Point3D(head.getTranslateX(), head.getTranslateY(), head.getTranslateZ());
        var halfHead = head.halfDimension();
        return checkInXDir(centerPoint3D, objectHalfDim, positionHead, halfHead) &&
                checkInYDir(centerPoint3D, objectHalfDim, positionHead, halfHead) &&
                checkInZDir(centerPoint3D, objectHalfDim, positionHead, halfHead);
    }

    private static boolean checkInZDir(Point3D centerPoint3D, Dimension3D objectHalfDim, Point3D positionHead, Dimension3D halfHead) {
        return positionHead.getZ() + halfHead.height() >= centerPoint3D.getZ() - objectHalfDim.depth() &&
                positionHead.getZ() - halfHead.height() <= centerPoint3D.getZ() + objectHalfDim.depth();
    }

    private static boolean checkInYDir(Point3D centerPoint3D, Dimension3D objectHalfDim, Point3D positionHead, Dimension3D halfHead) {
        return positionHead.getY() + halfHead.height() >= centerPoint3D.getY() - objectHalfDim.height() &&
                positionHead.getY() - halfHead.height() <= centerPoint3D.getY() + objectHalfDim.height();
    }

    private static boolean checkInXDir(Point3D centerPoint3D, Dimension3D objectHalfDim, Point3D positionHead, Dimension3D halfHead) {
        return positionHead.getX() + halfHead.width() >= centerPoint3D.getX() - objectHalfDim.width() &&
                positionHead.getX() - halfHead.width() <= centerPoint3D.getX() + objectHalfDim.width();
    }

    private boolean intersectsSelf() {
        return getChildren().stream()
                .filter(isEqual(head).negate())
                .map(Snake::toPoint3D)
                .anyMatch(this::inSnakeSegment);
    }

    private static Point3D toPoint3D(Node node) {
        return new Point3D(node.getTranslateX(), node.getTranslateY(), node.getTranslateZ());
    }

    public BodySegment getHead() {
        return head;
    }

    public Point3D getHeadTranslate() {
        return new Point3D(head.getTranslateX(), head.getTranslateY(), head.getTranslateZ());
    }

    public boolean isAlive() {
        return alive.get();
    }

    public void reset() {
        getChildren().clear();
        head.setTranslateX(0);
        head.setTranslateZ(0);
        segmentSize.set(INIT_SNAKE_SEGMENT_SIZE);
        getChildren().add(head);
        direction.set(INIT_DIRECTION);
        prevDirection = direction.get();
        buildBody(DEFAULT_NR_BODY_SEGMENTS);
        alive.set(true);
    }

    private boolean inSnakeSegment(Point3D centerPoint3D) {
        return objectInSnakeHead(centerPoint3D,
                new Dimension3D(segmentSize.get() / 2, segmentSize.get() / 2, segmentSize.get() / 2));
    }
}
