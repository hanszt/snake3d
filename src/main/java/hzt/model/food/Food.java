package hzt.model.food;

import hzt.model.Dimension3D;
import hzt.utils.RandomGenerator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point3D;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;

public abstract sealed class Food permits FoodBox, FoodCylinder, FoodSphere {

    private final PhongMaterial material;
    private final Shape3D shape3D;
    private final DoubleProperty size = new SimpleDoubleProperty();

    Food(Shape3D shape3D) {
        this.shape3D = shape3D;
        material = new PhongMaterial();
        material.setDiffuseColor(RandomGenerator.randomColor());
        shape3D.setMaterial(material);
        bindFoodSize();
    }

    public void setRandomColor() {
        material.setDiffuseColor(RandomGenerator.randomColor());
    }

    private void bindFoodSize() {
        bindSize();
    }

    abstract void bindSize();

    public void setTranslateXZ(double x, double z) {
        shape3D.setTranslateX(x);
        shape3D.setTranslateZ(z);
    }

    public Point3D getPosition() {
        return new Point3D(shape3D.getTranslateX(), shape3D.getTranslateY(), shape3D.getTranslateZ());
    }

    public Dimension3D getDimension() {
        return new Dimension3D(size.get(), size.get(), size.get());
    }

    public DoubleProperty translateYProperty() {
        return shape3D.translateYProperty();
    }

    public DoubleProperty sizeProperty() {
        return size;
    }

    public Shape3D getShape3D() {
        return shape3D;
    }

}
