package hzt.model.food;

import javafx.scene.shape.Box;

public final class FoodBox extends Food {


    public FoodBox() {
        super(new Box());
    }

    @Override
    void bindSize() {
        var box = ((Box) getShape3D());
        box.widthProperty().bind(sizeProperty());
        box.heightProperty().bind(sizeProperty());
        box.depthProperty().bind(sizeProperty());
    }
}
