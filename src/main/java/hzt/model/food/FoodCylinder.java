package hzt.model.food;

import javafx.scene.shape.Cylinder;

public final class FoodCylinder extends Food {

    public FoodCylinder() {
        super(new Cylinder());
    }

    @Override
    void bindSize() {
        var cylinder = (Cylinder) getShape3D();
        cylinder.heightProperty().bind(sizeProperty());
        cylinder.radiusProperty().bind(sizeProperty().divide(2));
    }
}
