package hzt.model.food;

import javafx.scene.shape.Sphere;

public final class FoodSphere extends Food {

    public FoodSphere() {
        super(new Sphere());
    }

    @Override
    void bindSize() {
        ((Sphere) getShape3D()).radiusProperty().bind(sizeProperty().divide(2));
    }
}
