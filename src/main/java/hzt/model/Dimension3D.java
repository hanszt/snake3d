package hzt.model;

import java.util.Objects;

public record Dimension3D(double width, double height, double depth) {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dimension3D that = (Dimension3D) o;
        return Double.compare(that.width, width) == 0
                && Double.compare(that.height, height) == 0
                && Double.compare(that.depth, depth) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(width, height, depth);
    }

    public Dimension3D multiply(double factor) {
        return new Dimension3D(this.width() * factor, this.height() * factor, this.depth() * factor);
    }
}
