package hzt.model;

import hzt.model.food.Food;
import hzt.model.food.FoodBox;
import hzt.model.food.FoodCylinder;
import hzt.model.food.FoodSphere;
import hzt.model.fxcustoms.inputtarget.Interactable;
import hzt.model.snake.BodySegment;
import hzt.model.snake.Snake;
import hzt.utils.RandomGenerator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;

import java.time.LocalTime;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Random;

public class World extends Group implements Interactable {

    private static final Random RANDOM = new Random();

    private final Deque<LocalTime> startTimes = new ArrayDeque<>();
    private final Box platform;
    private final Snake snake;
    private final List<Food> possibleFoods = List.of(new FoodSphere(), new FoodBox(), new FoodCylinder());
    private final ObjectProperty<Food> food = new SimpleObjectProperty<>();

    public World() {
        startTimes.push(LocalTime.now());
        snake = new Snake();
        var thickness = 4;
        this.platform = new Box(500, thickness, 500);
        platform.setTranslateY(thickness / 2.);
        platform.setMaterial(new PhongMaterial(Color.GREEN));
        possibleFoods.forEach(this::configureFood);
        this.food.set(pickRandomFoodShape());
        snake.reset();
        super.getChildren().addAll(platform, snake, food.get().getShape3D());
    }

    private void configureFood(Food food) {
        food.translateYProperty().bind(getSnake().segmentSizeProperty().divide(-2));
        food.sizeProperty().bind(snake.segmentSizeProperty());
    }

    private Food pickRandomFoodShape() {
        int randomIndex = RANDOM.nextInt(possibleFoods.size());
        var nextFood = possibleFoods.get(randomIndex);
        var positionX = RandomGenerator.randomDouble(platform.getTranslateX() - platform.getWidth() / 2,
                platform.getTranslateX() + platform.getWidth() / 2);
        var positionZ = RandomGenerator.randomDouble(platform.getTranslateZ() - platform.getDepth() / 2,
                platform.getTranslateZ() + platform.getDepth() / 2);
        nextFood.setTranslateXZ(positionX, positionZ);
        nextFood.setRandomColor();
        return nextFood;
    }

    public void update() {
        snake.update();
        boolean intersectsFood = snake.objectInSnakeHead(food.get().getPosition(), food.get().getDimension());
        if (intersectsFood) {
            snake.grow();
            getChildren().remove(food.get().getShape3D());
            this.food.set(pickRandomFoodShape());
            getChildren().add(food.get().getShape3D());
        }
        var head = snake.getHead();
        checkPlatformBounds(head);
    }

    private void checkPlatformBounds(BodySegment head) {
        checkBoundsInXDir(head);
        checkBoundsInZDir(head);
    }

    private void checkBoundsInZDir(BodySegment head) {
        double halfPlatformDepth = platform.getDepth() / 2;
        if (head.getTranslateZ() < platform.getTranslateZ() - halfPlatformDepth) {
            head.setTranslateZ(platform.getTranslateZ() + halfPlatformDepth);
        }
        if (head.getTranslateZ() > platform.getTranslateZ() + halfPlatformDepth) {
            head.setTranslateZ(platform.getTranslateZ() - halfPlatformDepth);
        }
    }

    private void checkBoundsInXDir(BodySegment head) {
        double halfPlatformWidth = platform.getWidth() / 2;
        if (head.getTranslateX() < platform.getTranslateX() - halfPlatformWidth) {
            head.setTranslateX(platform.getTranslateX() + halfPlatformWidth);
        }
        if (head.getTranslateX() > platform.getTranslateX() + halfPlatformWidth) {
            head.setTranslateX(platform.getTranslateX() - halfPlatformWidth);
        }
    }

    public Snake getSnake() {
        return snake;
    }

    public LocalTime getLastStartTime() {
        return startTimes.peek();
    }

    public void reset() {
        snake.reset();
    }
}
