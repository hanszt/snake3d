package hzt.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.util.Optional;

public abstract class FxmlController {

    private final Parent root;

    FxmlController(String fxmlResource) {
        try {
            var loader = new FXMLLoader();
            loader.setControllerFactory(c -> getController());
            var url = Optional.ofNullable(getClass().getResource(fxmlResource))
                    .orElseThrow();
            loader.setLocation(url);
            this.root = loader.load();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public Optional<Parent> getRoot() {
        return Optional.ofNullable(root);
    }

    abstract FxmlController getController();
}
