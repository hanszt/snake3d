package hzt.controller;

import hzt.controller.interfaces.IControlPaneController;
import hzt.model.snake.Snake;
import hzt.service.interfaces.IMouseControlService;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.Point3D;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;

public class ControlPaneController extends FxmlController implements IControlPaneController {

    private static final double INIT_ANGLE_X = 60;
    private static final double INIT_ANGLE_Y = 20;

    @FXML
    private ColorPicker snakeColorPicker;
    @FXML
    private Slider gameRateSlider;
    @FXML
    private Slider segmentSizeSlider;

    private final IMouseControlService mouseControlService;

    public ControlPaneController(IMouseControlService mouseControlService) {
        super("/fxml/controlsPane.fxml");
        this.mouseControlService = mouseControlService;
    }

    @Override
    FxmlController getController() {
        return this;
    }

    @Override
    public void setup(Timeline gameLoop, Snake snake) {
        segmentSizeSlider.setValue(Snake.INIT_SNAKE_SEGMENT_SIZE);
        snake.colorProperty().bind(snakeColorPicker.valueProperty());
        snake.segmentSizeProperty().bindBidirectional(segmentSizeSlider.valueProperty());
        snakeColorPicker.setValue(Color.RED);
        gameLoop.rateProperty().bindBidirectional(gameRateSlider.valueProperty());
        getRoot().ifPresent(root -> root.setStyle("-fx-base: rgb(40, 40, 40);-fx-background: rgb(60, 60, 60);"));
    }

    @Override
    public void center() {
        recenter();
    }

    @FXML
    private void recenter() {
        mouseControlService.setOrientation(INIT_ANGLE_X, INIT_ANGLE_Y);
        mouseControlService.setTargetTranslation(Point3D.ZERO);
    }

}
