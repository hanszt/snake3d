package hzt.controller.interfaces;

import hzt.model.World;
import hzt.service.interfaces.IMouseControlService;
import javafx.scene.Parent;

import java.util.Optional;

public interface IStatisticsController {

    void updateStats(World world, IMouseControlService mouseControlService);

    Optional<Parent> getRoot();

}
