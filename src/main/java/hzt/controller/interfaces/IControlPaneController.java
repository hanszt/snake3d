package hzt.controller.interfaces;

import hzt.model.snake.Snake;
import javafx.animation.Timeline;
import javafx.scene.Parent;

import java.util.Optional;

public interface IControlPaneController {

    void setup(Timeline gameLoop, Snake snake);

    void center();

    Optional<Parent> getRoot();
}
