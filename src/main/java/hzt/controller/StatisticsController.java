package hzt.controller;

import hzt.controller.interfaces.IStatisticsController;
import hzt.model.SogetiLogo;
import hzt.model.World;
import hzt.model.snake.Snake;
import hzt.service.interfaces.IMouseControlService;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.time.Duration;
import java.time.LocalDateTime;

import static java.lang.String.format;

public class StatisticsController extends FxmlController implements IStatisticsController {

    private static final String TWO_DEC_DOUBLE = "%4.2f";
    @FXML
    private VBox rootVbox;
    @FXML
    private Label lengthLabel;
    @FXML
    private Label positionXSnakeHeadLabel;
    @FXML
    private Label positionYSnakeHeadLabel;
    @FXML
    private Label positionZSnakeHeadLabel;
    @FXML
    private Label runtimeLabel;
    @FXML
    private Label positionXLabel;
    @FXML
    private Label positionYLabel;
    @FXML
    private Label rotationXLabel;
    @FXML
    private Label rotationYLabel;


    public StatisticsController() {
        super("/fxml/statisticsPane.fxml");
        SogetiLogo sogetiLogo = new SogetiLogo();
        sogetiLogo.setScale(.3);
        sogetiLogo.setTranslateX(-120);
        rootVbox.getChildren().add(sogetiLogo);
    }

    @Override
    public void updateStats(World world, IMouseControlService mouseControlService) {
        updateSnakeStats(world.getSnake());
        updateGeneralStats(world, mouseControlService);
    }

    private void updateGeneralStats(World world, IMouseControlService mouseControlService) {
        var runTime = Duration.between(world.getLastStartTime(), LocalDateTime.now());
        runtimeLabel.setText(String.format("%d.%03d", runTime.getSeconds(), runTime.toMillisPart()));

        positionXLabel.setText(format(TWO_DEC_DOUBLE, mouseControlService.getTargetTranslateX()));
        positionYLabel.setText(format(TWO_DEC_DOUBLE, mouseControlService.getTargetTranslateY()));

        rotationXLabel.setText(format(TWO_DEC_DOUBLE + " deg", mouseControlService.getTargetAngleX()));
        rotationYLabel.setText(format(TWO_DEC_DOUBLE + " deg", mouseControlService.getTargetAngleY()));
    }

    private void updateSnakeStats(Snake snake) {
        lengthLabel.setText(String.format("%d", snake.size()));
        var positionHead = snake.getHeadTranslate();
        positionXSnakeHeadLabel.setText(String.format(TWO_DEC_DOUBLE, positionHead.getX()));
        positionYSnakeHeadLabel.setText(String.format(TWO_DEC_DOUBLE, positionHead.getY()));
        positionZSnakeHeadLabel.setText(String.format(TWO_DEC_DOUBLE, positionHead.getZ()));
    }

    @Override
    FxmlController getController() {
        return this;
    }
}
