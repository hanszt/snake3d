package hzt.controller;

import hzt.controller.interfaces.IControlPaneController;
import hzt.controller.interfaces.IStatisticsController;
import hzt.model.World;
import hzt.model.fxcustoms.MyScene;
import hzt.model.fxcustoms.MyVBox;
import hzt.service.interfaces.IMouseControlService;
import hzt.utils.FxUtils;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static hzt.utils.FxUtils.getSceneFill;

public class SnakeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SnakeController.class);

    private static final double INIT_WORLD_FRAME_RATE = 8;
    public static final int INIT_SCENE_WIDTH = 800;
    public static final int INIT_SCENE_HEIGHT = 600;
    public static final int STATISTICS_PANE_WIDTH = 200;
    public static final int CONTROLS_PANE_WIDTH = 100;
    public static final int MIN_WIDTH = 600;
    public static final int MIN_HEIGHT = 600;

    private final Stage primaryStage;
    private final IMouseControlService mouseControlService;
    private final IStatisticsController statisticsController;
    private final IControlPaneController controlPaneController;
    private final GameOverDialogController gameOverDialogController;

    private final World world = new World();

    private final Timeline gameLoop;

    public SnakeController(Stage primaryStage,
                           IMouseControlService mouseControlService,
                           IStatisticsController statisticsController,
                           IControlPaneController controlPaneController) {
        this.primaryStage = primaryStage;
        this.mouseControlService = mouseControlService;
        this.statisticsController = statisticsController;
        this.controlPaneController = controlPaneController;
        this.gameLoop = FxUtils.buildTimeline("game loop", INIT_WORLD_FRAME_RATE, this::run);
        this.gameOverDialogController = new GameOverDialogController(primaryStage, world, gameLoop);
    }

    public void setup() {
        LOGGER.info("Snake3D starting...");
        MyVBox root = new MyVBox();
        root.setBackground(new Background(new BackgroundFill(getSceneFill(), CornerRadii.EMPTY, Insets.EMPTY)));
        final MyScene scene = configuredScene(root);
        scene.setOnKeyTyped(e -> keyTypedAction(e, world));

        HBox subSceneAndStatistics = getSubSceneAndStatistics(scene);
        mouseControlService.initMouseControl(world, root);
        root.getChildren().add(subSceneAndStatistics);
        Parent controlsRoot = getConfiguredControlsPane();
        root.getChildren().add(controlsRoot);
        configurePrimaryStage(scene);
        gameLoop.play();
        primaryStage.show();
        LOGGER.info("Snake 3D started");
    }

    @NotNull
    private HBox getSubSceneAndStatistics(MyScene scene) {
        final var subScene = configuredSubScene(world);
        subScene.widthProperty().bind(scene.widthProperty().subtract(STATISTICS_PANE_WIDTH));
        subScene.heightProperty().bind(scene.heightProperty().subtract(CONTROLS_PANE_WIDTH));

        var statisticsTimeline = FxUtils.buildTimeline("statistics loop", 30, this::updateStats);
        statisticsTimeline.play();
        var statisticsPane = statisticsController.getRoot().orElseThrow();
        return new HBox(subScene, statisticsPane);
    }

    private Parent getConfiguredControlsPane() {
        Parent controlsRoot = controlPaneController.getRoot().orElseThrow();
        controlPaneController.setup(gameLoop, world.getSnake());
        controlPaneController.center();
        return controlsRoot;
    }

    private void configurePrimaryStage(MyScene scene) {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Snake3D");
        primaryStage.setMinWidth(MIN_WIDTH);
        primaryStage.setMinHeight(MIN_HEIGHT);

        final var icons = primaryStage.getIcons();
        Optional.ofNullable(getClass().getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(icons::add);
    }

    private static void keyTypedAction(KeyEvent e, World world) {
        world.getSnake().updateDir(e);
        e.consume();
    }

    private void updateStats(ActionEvent event) {
        statisticsController.updateStats(world, mouseControlService);
    }

    private static MyScene configuredScene(Parent root) {
        return new MyScene(root, INIT_SCENE_WIDTH, INIT_SCENE_HEIGHT);
    }

    private static SubScene configuredSubScene(Group subSceneRoot) {
        SubScene subScene = new SubScene(subSceneRoot, 0, 0, true, SceneAntialiasing.BALANCED);
        PerspectiveCamera camera = new PerspectiveCamera(true);
        camera.setFarClip(10_000);
        camera.setTranslateZ(-1000);
        subScene.setCamera(camera);
        return subScene;
    }

    private void run(ActionEvent event) {
        world.update();
        if (!world.getSnake().isAlive()) {
            gameOverDialogController.showGameOverDialog();
        }
    }

}
