package hzt.controller;

import hzt.model.World;
import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.Deque;
import java.util.Optional;

public class GameOverDialogController {

    private final Deque<Score> scores = new ArrayDeque<>();
    private final Stage primaryStage;
    private final World world;
    private final Timeline gameLoop;

    public GameOverDialogController(Stage primaryStage, World world, Timeline gameLoop) {
        this.primaryStage = primaryStage;
        this.world = world;
        this.gameLoop = gameLoop;
    }

    void showGameOverDialog() {
        gameLoop.stop();
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        HBox buttonBox = buildButtons(dialog);
        Text gameOverText = new Text("Game over");
        gameOverText.setFont(Font.font(20));
        VBox scoreBox = buildScoreBox();
        VBox dialogVbox = new VBox(gameOverText, scoreBox, buttonBox);
        dialogVbox.setAlignment(Pos.CENTER);
        dialogVbox.setSpacing(5);
        Scene dialogScene = new Scene(dialogVbox, 300, 200);
        dialog.setScene(dialogScene);
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.show();
    }

    private VBox buildScoreBox() {
        Score curScore = new Score(world.getSnake().size());
        boolean isHighScore = isHighScore(curScore);
        String text = isHighScore ? "New high score: " : "Score: ";
        Text scoreText = new Text(text + curScore.scoreAsString());
        scoreText.setFont(new Font(15));
        if (isHighScore) {
            addFillTransition(scoreText);
        }
        Text prevScoresText = new Text("Previous scores:");
        ListView<String> listView = new ListView<>();
        scores.forEach(score -> listView.getItems().add(score.scoreAsString()));
        scores.push(curScore);
        Text highScoreText = new Text("Highscore: " + getHighScore().orElse(curScore).scoreAsString());
        VBox vBox = new VBox(scoreText, highScoreText, prevScoresText, listView);
        vBox.setAlignment(Pos.CENTER);
        return vBox;
    }

    private static void addFillTransition(Text scoreText) {
        var fillTransition = new FillTransition(Duration.seconds(1),
                scoreText, Color.LIGHTGREEN, Color.DARKBLUE);
        fillTransition.setAutoReverse(true);
        fillTransition.setCycleCount(Animation.INDEFINITE);
        fillTransition.play();
    }

    private Optional<Score> getHighScore() {
        return scores.stream().max(Comparator.comparing(score -> score.value));
    }

    private boolean isHighScore(Score curScore) {
        return scores.stream().noneMatch(score -> score.value >= curScore.value);
    }

    @NotNull
    private HBox buildButtons(Stage dialog) {
        var againButton = new Button("Again!");
        var exitButton = new Button("Exit");
        exitButton.setOnAction(e -> primaryStage.close());
        againButton.setOnAction(e -> restart(dialog));
        againButton.setOnMouseEntered(e -> againButton.setCursor(Cursor.HAND));
        HBox buttonBox = new HBox(againButton, exitButton);
        buttonBox.setSpacing(5);
        buttonBox.setAlignment(Pos.CENTER);
        return buttonBox;
    }

    private void restart(Stage dialog) {
        world.reset();
        dialog.close();
        gameLoop.setRate(1);
        gameLoop.play();
    }

    private record Score(double value) {

        private String scoreAsString() {
            return String.valueOf(value);
        }
    }
}
