package hzt.service.interfaces;

import hzt.model.fxcustoms.inputtarget.MouseTarget;
import javafx.geometry.Point3D;
import javafx.scene.Node;

public interface IMouseControlService {

    void initMouseControl(Node target, MouseTarget reference);

    void setOrientation(double initAngleX, double initAngleY);

    void setTargetTranslation(Point3D point);

    double getTargetTranslateX();

    double getTargetTranslateY();

    double getTargetTranslateZ();

    double getTargetAngleX();

    double getTargetAngleY();
}
