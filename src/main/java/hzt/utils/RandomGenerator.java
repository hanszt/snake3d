package hzt.utils;

import javafx.geometry.Point3D;
import javafx.scene.paint.Color;

import static java.lang.Math.*;

public final class RandomGenerator {

    private RandomGenerator() {
    }

    public static double randomDouble(double lowerBound, double upperBound) {
        if (lowerBound > upperBound) {
            throw new IllegalStateException("lowerBound greater than upper bound");
        }
        double diff = upperBound - lowerBound;
        return random() * diff + lowerBound;
    }

    public static Color randomColor() {
        return Color.color(random(), random(), random());
    }

    public static Point3D randomAngle() {
        return new Point3D(random(), random(), random());
    }
}
