package hzt.utils;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.Shape3D;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public final class FxUtils {

    private FxUtils() {
    }

    public static List<Node> allDescendents(Parent root) {
        List<Node> nodes = new ArrayList<>();
        addAllDescendents(root, nodes);
        return nodes;
    }

    private static void addAllDescendents(Parent parent, List<Node> nodes) {
        for (Node node : parent.getChildrenUnmodifiable()) {
            nodes.add(node);
            if (node instanceof Parent childParent) {
                addAllDescendents(childParent, nodes);
            }
        }
    }

    public static Timeline buildTimeline(String name, double frameRate, EventHandler<ActionEvent> loopEventHandler) {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1. / frameRate), name, loopEventHandler));
        timeline.setCycleCount(Animation.INDEFINITE);
        return timeline;
    }

    public static void setDrawMode(Shape3D shape3D) {
        shape3D.setDrawMode(shape3D.getDrawMode() == DrawMode.LINE ? DrawMode.FILL : DrawMode.LINE);
    }

    public static RadialGradient getSceneFill() {
        return new RadialGradient(225, 0.85, 300, 300, 500, false,
                CycleMethod.NO_CYCLE, new Stop(0F, Color.BLUE), new Stop(1F, Color.LIGHTBLUE));
    }

    public static <N> ChangeListener<N> newValue(Consumer<N> consumer) {
        return (o, c, n) -> consumer.accept(n);
    }

    public static EventHandler<ActionEvent> consuming(Runnable runnable) {
        return event -> {
            runnable.run();
            event.consume();
        };
    }

    public static EventHandler<ActionEvent> multiple(Runnable... runnables) {
        return event -> {
            for (Runnable runnable : runnables) {
               runnable.run();
            }
            event.consume();
        };
    }

    public static void addIconToStage(Stage primaryStage) {
        Optional.ofNullable(FxUtils.class.getResourceAsStream("/icons/fx-icon.png"))
                .map(Image::new)
                .ifPresent(primaryStage.getIcons()::add);
    }
}
