package hzt.utils.builders.shape;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;

public record EllipseBuilder(Ellipse ellipse) {

    public static EllipseBuilder ellipse(double radiusX, double radiusY) {
        return new EllipseBuilder(new Ellipse(radiusX, radiusY));
    }

    public EllipseBuilder withFill(Paint paint) {
        ellipse.setFill(paint);
        return this;
    }

    public EllipseBuilder withTranslation(double x, double y) {
        ellipse.setTranslateX(x);
        ellipse.setTranslateY(y);
        return this;
    }

    public Ellipse build() {
        return ellipse;
    }
}
