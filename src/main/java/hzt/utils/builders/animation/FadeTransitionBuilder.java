package hzt.utils.builders.animation;

import javafx.animation.FadeTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public final class FadeTransitionBuilder extends Transitions {

    FadeTransitionBuilder(Duration duration) {
        super(new FadeTransition(duration));
    }

    public FadeTransitionBuilder forNode(Node node) {
        ((FadeTransition) transition).setNode(node);
        return this;
    }

    public FadeTransitionBuilder fromValue(double fromValue) {
        ((FadeTransition) transition).setFromValue(fromValue);
        return this;
    }

    public FadeTransitionBuilder toValue(double toValue) {
        ((FadeTransition) transition).setToValue(toValue);
        return this;
    }

    @Override
    public FadeTransition buildAndPlay() {
        play();
        return (FadeTransition) transition;
    }

    @Override
    public FadeTransition build() {
        return (FadeTransition) transition;
    }
}
