package hzt.utils.builders.animation;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

public abstract sealed class Transitions extends Animations permits
        FillTransitionBuilder, FadeTransitionBuilder, RotateTransitionBuilder, TranslateTransitionBuilder {

    protected final Transition transition;

    protected Transitions(Transition transition) {
        super(transition);
        this.transition = transition;
    }

    public Transitions withInterpolator(Interpolator interpolator) {
        transition.setInterpolator(interpolator);
        return this;
    }

    public static RotateTransitionBuilder rotateTransition(Duration duration, Node node) {
        return new RotateTransitionBuilder(duration, node);
    }

    public static TranslateTransitionBuilder translateTransition(Duration duration, Node node) {
        return new TranslateTransitionBuilder(new TranslateTransition(duration, node));
    }

    public static FillTransitionBuilder fillTransition(Duration duration) {
        return new FillTransitionBuilder(duration);
    }

    public static FadeTransitionBuilder fadeTransition(Duration duration) {
        return new FadeTransitionBuilder(duration);
    }

    public abstract Transition buildAndPlay();

    public abstract Transition build();
}
