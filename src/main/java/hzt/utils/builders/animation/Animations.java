package hzt.utils.builders.animation;

import javafx.animation.Animation;

public abstract sealed class Animations permits Transitions {

    private final Animation animation;

    protected Animations(Animation animation) {
        this.animation = animation;
    }

    public Animations withCycleCount(int cycleCount) {
        animation.setCycleCount(cycleCount);
        return this;
    }

    public Animations withAutoReverse(boolean autoReverse) {
        animation.setAutoReverse(autoReverse);
        return this;
    }

    public void play() {
        animation.play();
    }

    public abstract Animation buildAndPlay();

    public abstract Animation build();
}
