package hzt.utils.builders.animation;

import javafx.animation.TranslateTransition;

public final class TranslateTransitionBuilder extends Transitions {

    private final TranslateTransition translateTransition;

    TranslateTransitionBuilder(TranslateTransition translateTransition) {
        super(translateTransition);
        this.translateTransition = translateTransition;
    }

    public TranslateTransitionBuilder fromX(double fromX) {
        translateTransition.setFromX(fromX);
        return this;
    }

    public TranslateTransitionBuilder fromY(double fromY) {
        translateTransition.setFromY(fromY);
        return this;
    }

    public TranslateTransitionBuilder fromZ(double fromZ) {
        translateTransition.setFromZ(fromZ);
        return this;
    }

    public TranslateTransitionBuilder toX(double toX) {
        translateTransition.setToX(toX);
        return this;
    }

    public TranslateTransitionBuilder toY(double toY) {
        translateTransition.setToY(toY);
        return this;
    }

    public TranslateTransitionBuilder toZ(double toZ) {
        translateTransition.setToZ(toZ);
        return this;
    }


    @Override
    public TranslateTransition buildAndPlay() {
        play();
        return translateTransition;
    }

    @Override
    public TranslateTransition build() {
        return translateTransition;
    }
}
