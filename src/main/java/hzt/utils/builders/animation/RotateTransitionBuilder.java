package hzt.utils.builders.animation;

import javafx.animation.RotateTransition;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.util.Duration;

public final class RotateTransitionBuilder extends Transitions {

    RotateTransitionBuilder(Duration duration, Node node) {
        super(new RotateTransition(duration, node));
    }

    public RotateTransitionBuilder withAxis(Point3D axis) {
        ((RotateTransition) transition).setAxis(axis);
        return this;
    }

    public RotateTransitionBuilder fromAngle(double fromAngle) {
        ((RotateTransition) transition).setFromAngle(fromAngle);
        return this;
    }

    public RotateTransitionBuilder toAngle(double toAngle) {
        ((RotateTransition) transition).setToAngle(toAngle);
        return this;
    }

    @Override
    public RotateTransition buildAndPlay() {
        play();
        return (RotateTransition) transition;
    }

    @Override
    public RotateTransition build() {
        return (RotateTransition) transition;
    }
}
