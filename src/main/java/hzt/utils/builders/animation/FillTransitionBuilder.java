package hzt.utils.builders.animation;

import javafx.animation.FillTransition;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

public final class FillTransitionBuilder extends Transitions {

    FillTransitionBuilder(Duration duration) {
        super(new FillTransition(duration));
    }

    public FillTransitionBuilder forShape(Shape shape) {
        ((FillTransition) transition).setShape(shape);
        return this;
    }

    public FillTransitionBuilder fromValue(Color color) {
        ((FillTransition) transition).setFromValue(color);
        return this;
    }

    public FillTransitionBuilder toValue(Color color) {
        ((FillTransition) transition).setToValue(color);
        return this;
    }

    @Override
    public FillTransition buildAndPlay() {
        play();
        return (FillTransition) transition;
     }

    @Override
    public FillTransition build() {
        return (FillTransition) transition;
    }
}
