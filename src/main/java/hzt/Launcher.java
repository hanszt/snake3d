package hzt;

import hzt.controller.SnakeController;
import hzt.controller.StatisticsController;
import hzt.controller.ControlPaneController;
import hzt.service.interfaces.IMouseControlService;
import hzt.service.MouseControlService;
import javafx.application.Application;
import javafx.stage.Stage;

public class Launcher extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) {
        IMouseControlService mouseControlService = new MouseControlService();
        new SnakeController(stage,
                mouseControlService,
                new StatisticsController(),
                new ControlPaneController(mouseControlService)).setup();
    }

}
